-> Spring Boot comes with over 50+ different starter modules, which provide ready-to-use integration libraries for many different frameworks

-> For eg : If we include spring-boot-starter-data-jpa in our build as a dependency, we will automatically get spring-orm, hibernate-entity-manager and spring-data-jpa.

-> 

#Curl Requests:

#Fetch All Users

curl -X GET \
  http://localhost:9010/v1/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: f34a7d00-ab93-3465-ecb1-201ea5ed28ef' \
  -d '{
	"id":4,
	"firstName":"asandsad",
	"lastName":"agarwal",
	"email":"xyz@gmail.com"
}'

#Fetch user by id

curl -X GET \
  http://localhost:9010/v1/fetch/1 \
  -H 'cache-control: no-cache' \
  -H 'postman-token: f0d53f71-0b09-1a0d-3308-f4900823d314'
  
  
#Update User
 curl -X PUT \
  http://localhost:9010/v1/user/update \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: bde44744-abe8-9d9e-7905-e3b48dc6bcc5' \
  -d '{
	"id":1,
	"firstName":"afkjsasd",
	"lastName":"awqewqe",
	"email":"xyz@gmail.com"
}'

#Create User

curl -X POST \
  http://localhost:9010/v1/user/ \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 12e84e7b-db9d-81c3-30ff-5c81eb0222cb' \
  -d '{
	"id":5,
	"firstName":"test",
	"lastName":"awqewqe",
	"email":"xyz@gmail.com"
}'


#Delete User
curl -X DELETE \
  http://localhost:9010/v1/delete/5 \
  -H 'cache-control: no-cache' \
  -H 'postman-token: c7dc7703-9153-908e-0cd7-4a3e2bd9a744'
