package com.practice.dao;

import java.util.List;

import com.practice.model.User;

public interface UserDao {
	

	List<User> getAllUsers();

	String addUser(User user);

	User fetchUserById(String id);

	User update(User user);

	void delete(User user);


}
