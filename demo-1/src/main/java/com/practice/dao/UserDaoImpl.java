package com.practice.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.practice.model.User;


@Transactional
@Repository
public class UserDaoImpl implements UserDao{
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession() {
		Session session=sessionFactory.getCurrentSession();
		if(session==null) {
			session=sessionFactory.openSession();
		}
		return session;
	}


	@SuppressWarnings({"unchecked", "deprecation" })
	@Override
	public List<User> getAllUsers() {
		List<User> ls=getSession().createCriteria(User.class).list();
		return ls;
	}

	@Override
	public String addUser(User user) {
		user.setModifiedDate(new Date());
		getSession().save(user);
		return String.valueOf(user.getId());
	}

	@Override
	public User fetchUserById(String id) {
		User user=getSession().get(User.class, Long.valueOf(id));
		return user;
	}
	
	@Override
	public User update(User user) {
		user.setModifiedDate(new Date());
		return (User) getSession().merge(user);
	}


	@Override
	public void delete(User user) {
		getSession().delete(user);
	}
}
