package com.practice.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.exception.ResourceNotFoundException;
import com.practice.model.User;
import com.practice.service.UserService;

@RestController
@RequestMapping(path = "/v1")
public class UserController {

	private static final Logger log=LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@GetMapping(path="/",produces="application/json")
	public List<User> fetchAllUsers() {
		log.info("Fetching All users");
		return userService.getAllUsers();
	}
	
	//Fetch User by ID
	@GetMapping(path="/fetch/{userId}",produces="application/json")
	public User fetchUserById(@PathVariable("userId")String userId ) {
		User user= userService.fetchUserById(userId);
		if(user!=null) {
			log.info("Fetching user by id = {}",userId);
			return user;
		}
		log.info("User Not Found with id = {}", userId);
		throw new ResourceNotFoundException("user", "id", userId)	;	
	}
	
	//Create User 
	@PostMapping(path="/user",produces="application/json",consumes = "application/json")
	public Map<String,String> addUser(@Valid @RequestBody User user){
		Map<String,String> mp=new HashMap<>();
		//Check for duplicate Users
		User user2= userService.fetchUserById(String.valueOf(user.getId()));
		if(user2!=null) {
			mp.put("message", "User Already Present");
			return mp;
		}
		log.info("Creating user of id = {}",user.getId());
        String userId=userService.addUser(user);
        mp.put("userId", userId);
		return mp;
	}
	
	//update User
	@PutMapping(path="/user/update",consumes="application/json",produces="application/json")
	public ResponseEntity<Object> updateUser(@Valid @RequestBody User user){
		User user2= userService.fetchUserById(String.valueOf(user.getId()));
		if(user2!=null) {
			log.info("Updating user of id = {}",user.getId());
			user2=userService.update(user);
			return new ResponseEntity<Object>(user2, HttpStatus.OK);
		}
		throw new ResourceNotFoundException("user", "id", user.getId())	;	
	}
	
	// Delete User by ID
	@DeleteMapping(path = "/delete/{userId}")
	public Map<String,String> DelUserById(@PathVariable("userId") String userId) {
		
		Map<String,String> mp=new HashMap<>();
		User user = userService.fetchUserById(userId);
		if (user != null) {
			log.info("Delete user of id = {}",user.getId());
			userService.delete(user);
			mp.put("status","success");
			return mp;
		}else {
			throw new ResourceNotFoundException("user", "id", userId);
		}
	}
}
