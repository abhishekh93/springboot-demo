package com.practice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.practice.model.User;

@Service
public interface UserService {

	List<User> getAllUsers();

	String addUser(User user);

	User fetchUserById(String id);

	User update(User user);

	void delete(User user);

}
