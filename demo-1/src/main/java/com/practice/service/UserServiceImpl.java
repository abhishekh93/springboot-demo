package com.practice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.dao.UserDao;
import com.practice.model.User;

@Service
public class UserServiceImpl implements UserService{

	
	@Autowired
	UserDao userDao;
	
	
	@Override
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

	@Override
	public String addUser(User user) {
		return userDao.addUser(user);
	}

	@Override
	public User fetchUserById(String id) {
		return userDao.fetchUserById(id);
	}
	
	@Override
	public User update(User user) {
		return userDao.update(user);
	}

	@Override
	public void delete(User user) {
		userDao.delete(user);
		
	}
	

}
