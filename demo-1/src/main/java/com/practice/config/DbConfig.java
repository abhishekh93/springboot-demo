package com.practice.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@Configuration
public class DbConfig {

	@Value("${spring.datasource.driver-class-name}")
	private String jdbcDriver;
	
	@Value("${spring.datasource.url}")
	private String url;
	
	@Value("${spring.datasource.username}")
	private String username;
	
	@Value("${spring.datasource.password}")
	private String password;
	
	@Value("${hibernate-dialect}")
	private String dialect;
	
	@Bean
	public DataSource getDataSource() {
        @SuppressWarnings("rawtypes")
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(jdbcDriver);
        dataSourceBuilder.url(url);
        dataSourceBuilder.username(username);
        dataSourceBuilder.password(password);
        return dataSourceBuilder.build();
    }
	
	@Bean
	public LocalSessionFactoryBean sessionfactory() {
		LocalSessionFactoryBean factory=new LocalSessionFactoryBean();
		factory.setDataSource(getDataSource());
		factory.setHibernateProperties(HibProperties());
		factory.setPackagesToScan(new String[] {"com.practice.model"});
		return factory;
	}
	
	private Properties HibProperties() {
		Properties properties=new Properties();
		properties.put("hibernate-dialect", dialect);
		properties.put("hibernate.ddl-auto", "update");
		properties.put("hibernate.show_sql", "false");
		properties.put("hibernate.format_sql", "true");
		return properties;
	}
	
	@Bean
	@Autowired
	public HibernateTransactionManager TransactionManager(SessionFactory factory) {
		HibernateTransactionManager transactionManager=new HibernateTransactionManager();
		transactionManager.setSessionFactory(factory);
		return transactionManager;
	}

}
