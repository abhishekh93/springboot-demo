Demo project in spring boot:

User Registration Service:

Curls for differnt API requests:



curl -X GET \
  http://localhost:9010/v1/fetch/1 \
  -H 'cache-control: no-cache' \
  -H 'postman-token: 07fb5713-4ed6-66b1-fcc0-bebd8ce896c1'



curl -X PUT \
  http://localhost:9010/v1/user/update \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: e2c8e263-77a9-ce2d-9a55-3c41d0e67b13' \
  -d '{
	"id":2,
	"firstName":"xyz",
	"lastName":"agarwal",
	"email":"xyz@gmail.com"
}'

curl -X DELETE \
  http://localhost:9010/v1/delete/1 \
  -H 'cache-control: no-cache' \
  -H 'postman-token: d4180b3b-9d7a-5204-3758-c7fc56c8244b'



